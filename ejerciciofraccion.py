class Fraccion:
    def __init__ (self, n = 0, d = 1): #atributos: elementos de la clase (en el caso de fracción tiene un numerador o denomidor), podemos poner o no valores por defecto.
        self.numerador = n 
        self. denominador = d

    def sumar_fraccion(izq, drch): # FUNCIÓN : metemos datos y nos devuelve otra cosa.
        return Fraccion(izq.numerador * drch.denominador + izq.denominador * drch.numerador, drch.denominador * izq.denominador)

    def suma(self, otro):#MÉTODO: altera los objetos que ya teníamos.
        self.numerador = self.numerador * otro.denominador +  self.denominador * otro.numerador
        self.denominador= self.denominador * otro.denominador
        return self
    #def resta_fraccion(self):

    #def multiplicacin_fraccion(self):
    
    #def division_fraccion(self):


#Dos formas de definir fracciones def__init__(self, n=0,d=1):
unmedio = Fraccion (1, 2) # Según hemos definido el objeto de clase Fraccion, cada objeto tiene (n,d) ponemos n=1, d=2 y nos devuelve la fracción 1/2.
uno = Fraccion (1) #Según hemos definido el objeto de clase Fraccion.En este caso, n=1 y como no hemos puesto valor para d, tomamos el valor por defecto d=1, luego nos da 1/1=1.
cero= Fraccion () #Según hemos definido el objeto de clase Fraccion.En este caso, n y d no están definidos por lo que tomamos el valor por defecto tomamos el valor por defecto n=0 y d=1, luego nos da 0/1=0.

#Dos formas de sumar
#mi_suma = uno + unmedio
mi_suma1 =Fraccion.sumar_fraccion(uno, unmedio) #función
mi_suma2 = uno.suma(unmedio)
print(mi_suma1)
print(mi_suma2)